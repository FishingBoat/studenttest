#include <iostream>
#include "Student.h"
using namespace std;

int main()
{
    int n;
    cout<<"Enter num of students: ";
    cin>>n;
    Student* stdList=new Student[n];
    string name;
    string hobby;
    int stdNum;
    for(int i=0;i<n;i++)
    {
        cout<<"Student number "<<i+1<<":"<<endl;
        cout<<"Enter student num: ";
        cin>>stdNum;
        cout<<"Enter student name: ";
        cin>>name;
        cout<<"Enter student hobby: ";
        cin>>hobby;
        //a one time per student pseudoConstructor to make dynamic allocation not insane
        stdList[i].init(name,stdNum,hobby);
    }
    //to test for set and get operation
    if(n>=1)
    {
        stdList[n-1].printStudent();
        stdList[n-1].setHobby("DETENTION");
        stdList[n-1].setName("MANDY");
        stdList[n-1].printStudent();
    }

    delete stdList;
    return 0;
}
