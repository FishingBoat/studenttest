#include <iostream>

using namespace std;

class Student
{
private:
    int id;
    string hobby;
    string name;
public:
    Student();
    Student(Student&);
    Student(int,string,string);
    string getHobby(){return hobby;}
    string getName(){return name;}
    int getId(){return id;}
    void setHobby(string newhobby){hobby=newhobby;}
    void setName(string newname){name=newname;}
    void printStudent();
    //for late initialization for dynamic allocation
    void init(string,int,string);
};
