#include <iostream>
#include "Student.h"
using namespace std;
//default constructor, init can be used if id is still -1 since obviously a static id
//when uninitialized is useless
Student::Student()
{
    name="";
    hobby="";
    id=-1;
}
//the copy constructor, had some trouble with it giving me
//weird messages like discarding qualifiers or something
Student::Student(Student& other)
{
    this->name=other.getName();
    this->hobby=other.getHobby();
    this->id=other.getId();
}
//the overload constructor where we give them everything
Student::Student(int newID, string newName, string newHobby)
{
    hobby=newHobby;
    name=newName;
    id=newID;
}
//if student has been initialize with default constructor then we can still fill them out with the student ID and all
//this is necessary to give dynamically allocated students details after allocation
void Student::init(string newName,int newId,string newHobby)
{
    if(id==-1)
    {
        name=newName;
        id=newId;
        hobby=newHobby;
    }
    else cout<<"Object initalized, improper call to init."<<endl;
}
void Student::printStudent()
{
    cout<<"Student name: "<<name<<endl
    <<"ID: "<<id<<endl<<"Hobby: "<<hobby<<endl;
}
